import type { ManifestOptions } from 'vite-plugin-pwa';

const isDev = process.env.NODE_ENV === 'dev';

export default {
  short_name: 'MOSK',
  name: 'Money Task',
  icons: [
    {
      src: '/logo24.svg',
      sizes: '24x24',
      type: 'image/svg',
    },
    {
      src: '/logo64.svg',
      sizes: '64x64',
      type: 'image/svg',
    },
    {
      src: '/logo192.svg',
      type: 'image/svg',
      sizes: '192x192',
    },
    {
      src: '/preview.png',
      type: 'image/png',
      sizes: '512x512',
    },
  ],
  start_url: './',
  display: 'standalone',
  theme_color: '#1a1d2d',
  background_color: '#1a1d2d',
  related_applications: [
    {
      platform: 'webapp',
      url: isDev ? 'http://localhost:5173/manifest.webmanifest' : 'https://money-task.ru/manifest.webmanifest',
    },
  ],
} satisfies Partial<ManifestOptions>;
