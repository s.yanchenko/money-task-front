FROM node:18.20.2-alpine as build
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build

FROM nginx:stable-alpine
#CMD ["rm", "-rf", "/var/www/dist"]
COPY --from=build /app/dist /var/www
COPY --from=build /app/.nginx/nginx.conf /etc/nginx/sites-enabled/default
EXPOSE 80
#CMD ["sudo", "service", "nginx", "restart"]
CMD ["nginx", "-g", "daemon off;"]