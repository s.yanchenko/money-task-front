import { ReactNode } from 'react';

type BadgeProps = {
  children: ReactNode;
  color?: 'gray' | 'red' | 'yellow' | 'green' | 'blue' | 'indigo' | 'purple' | 'pink';
  size?: 'xs' | 'sm' | 'base' | 'lg';
};

const colorVariants = {
  gray: 'bg-gray-50 text-gray-600 ring-gray-500/10',
  red: 'bg-red-50 text-red-700 ring-red-600/10',
  yellow: 'bg-yellow-50 text-yellow-800 ring-yellow-600/20',
  green: 'bg-green-200 text-green-600 ring-green-200/20',
  blue: 'bg-blue-50 text-blue-700 ring-blue-700/10',
  indigo: 'bg-indigo-50 text-indigo-700 ring-indigo-700/10',
  purple: 'bg-purple-50 text-purple-700 ring-purple-700/10',
  pink: 'bg-pink-50 text-pink-700 ring-1 ring-inset ring-pink-700/10',
};

const sizeVariants = {
  xs: 'text-xs',
  sm: 'text-sm',
  base: 'text-base',
  lg: 'text-lg',
};

const Badge = ({ children, color = 'gray', size = 'sm' }: BadgeProps) => {
  return (
    <span
      className={`${colorVariants[color]} ${sizeVariants[size]} inline-flex items-center rounded-md px-2 py-1  font-semibold ring-1 ring-inset`}
    >
      {children}
    </span>
  );
};

export default Badge;
