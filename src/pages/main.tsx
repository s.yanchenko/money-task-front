import Badge from '@/shared/Badge';

const MainPage = () => {
  return (
    <div>
      <Badge color="green">Main</Badge>
    </div>
  );
};

export default MainPage;
