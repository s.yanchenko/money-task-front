import { lazy, Suspense } from "react";
import { createBrowserRouter } from "react-router-dom";
import { Paths } from "@/constants/paths";

const LazyMainPage = lazy(() => import("@/pages/main"));

export const router = createBrowserRouter([
  {
    path: Paths.MAIN,
    element: (
      <Suspense fallback={<div>fallback</div>}>
        <LazyMainPage />
      </Suspense>
    ),
  },
  {
    path: "*",
    element: <div>No match</div>,
  },
]);
